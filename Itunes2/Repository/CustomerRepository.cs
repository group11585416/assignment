﻿using Itunes2.Model;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Repository
{
    public class CustomerRepository : ICustomerRepository
    {

        public List<Customer> GetAllCustomers()
        {
            //To get all professors from the database
            List<Customer> CustomerList = new List<Customer>();
            //Select statement
            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                //Assign value from database to the ID
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                                //Add to the collection List.

                                // string sstatus = reader.IsDBNull(0) ? null :
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return CustomerList;






        }

        public Customer GetcustomerByID(int id)
        {
            Customer customer = new Customer();

            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where CustomerId=@ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                //Assign value from database to the ID
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        public Customer GetcustomerByName(string Name)
        {

            Customer customer = new Customer();

            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where LastName=@Name";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Name", Name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                //Assign value from database to the ID
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? null : reader.GetString(6);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;


        }
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "Insert into Customer( FirstName, LastName, Company, Address, City, State, Country, PostalCode,Phone, Fax, Email)" +
                " Values(@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // cmd.Parameters.AddWithValue("1", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@2", customer.FirstName);
                        cmd.Parameters.AddWithValue("@3", customer.LastName);
                        cmd.Parameters.AddWithValue("@4", customer.Company);
                        cmd.Parameters.AddWithValue("@5", customer.Address);
                        cmd.Parameters.AddWithValue("@6", customer.City);
                        cmd.Parameters.AddWithValue("@7", customer.State);
                        cmd.Parameters.AddWithValue("@8", customer.Country);
                        cmd.Parameters.AddWithValue("@9", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@10", customer.Phone);
                        cmd.Parameters.AddWithValue("@11", customer.Fax);
                        cmd.Parameters.AddWithValue("@12", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }



        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "Update Customer set FirstName=@2, LastName=@3, Company=@4, Address=@5, City=@6, State=@7, Country=@8, PostalCode=@9,Phone=@10, Fax=@11, Email=@12" +
                " where CustomerId=@ID";
            ;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@2", customer.FirstName);
                        cmd.Parameters.AddWithValue("@3", customer.LastName);
                        cmd.Parameters.AddWithValue("@4", customer.Company);
                        cmd.Parameters.AddWithValue("@5", customer.Address);
                        cmd.Parameters.AddWithValue("@6", customer.City);
                        cmd.Parameters.AddWithValue("@7", customer.State);
                        cmd.Parameters.AddWithValue("@8", customer.Country);
                        cmd.Parameters.AddWithValue("@9", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@10", customer.Phone);
                        cmd.Parameters.AddWithValue("@11", customer.Fax);
                        cmd.Parameters.AddWithValue("@12", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        public List<CustomerAmountByCountry> GetCustomerByCountry()
        {
            //To get all professors from the database
            List<CustomerAmountByCountry> CustomerList = new List<CustomerAmountByCountry>();
            //Select statement
            string sql = "SELECT COUNT(CustomerID), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC;";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerAmountByCountry temp = new CustomerAmountByCountry();
                                //Assign value from database to the ID
                                temp.Amount = reader.GetInt32(0);
                                temp.Country = reader.GetString(1);

                                //Add to the collection List.

                                // string sstatus = reader.IsDBNull(0) ? null :
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return CustomerList;

        }

        public List<CostumerSpendings> GetCustomerSpending()
        {
            List<CostumerSpendings> CustomerList = new List<CostumerSpendings>();
            //Select statement
            string sql = "SELECT customer.CustomerId, Customer.FirstName, customer.LastName, Invoice.Total FROM Customer INNER JOIN Invoice ON Invoice.CustomerId=Customer.CustomerId " +
                "ORDER BY Invoice.Total DESC;";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CostumerSpendings temp = new CostumerSpendings();
                                //Assign value from database to the ID
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Total = reader.GetDecimal(3);

                                //Add to the collection List.

                                // string sstatus = reader.IsDBNull(0) ? null :
                                CustomerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return CustomerList;

        }
        public List<SpendingsGenre> GetMostpopularGenre(int id)
        {
            List<SpendingsGenre> Spendinglist = new List<SpendingsGenre>();
            //Select statement
            string sql = "SELECT SUM(Invoice.Total),Genre.Name FROM Customer INNER JOIN Invoice ON Invoice.CustomerId=Customer.CustomerId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId=Invoice.InvoiceId INNER JOIN Track ON InvoiceLine.TrackId=track.TrackId " +
                "INNER JoIN Genre ON track.GenreId=Genre.GenreId Where customer.CustomerId=@ID group by genre.Name order by SUM(Invoice.Total) desc;";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SpendingsGenre temp = new SpendingsGenre();
                                //Assign value from database to the ID
                                temp.Spending = reader.GetDecimal(0);
                                temp.Genre = reader.GetString(1);
                                

                                //Add to the collection List.

                                // string sstatus = reader.IsDBNull(0) ? null :
                                Spendinglist.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return Spendinglist;
        }
    }



}
