﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Repository
{
    public static class ConnectionHelper
    {

        public static string GetConnectionString()
        {
            //ConnectionStringBuilder - created connection string buildiner.
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "DESKTOP-4M6OM7Q\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity = true;
            connectStringBuilder.TrustServerCertificate = true;
            return connectStringBuilder.ConnectionString;
        }



    }
}
