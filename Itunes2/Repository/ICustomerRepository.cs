﻿using Itunes2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Repository
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();
        public Customer GetcustomerByID(int id);
        public Customer GetcustomerByName(string Name);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerAmountByCountry> GetCustomerByCountry();

        public List<CostumerSpendings> GetCustomerSpending();
        public List<SpendingsGenre> GetMostpopularGenre(int id);




    }
}
