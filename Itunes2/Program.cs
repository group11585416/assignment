﻿using Itunes2.Model;
using System;
using System.Collections.Generic;
using Itunes2.Repository;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Data.SqlClient;

namespace Itunes2
{
    public class Program
    {
        static void Main(string[] args)
        {


            ICustomerRepository repository = new CustomerRepository();

            //    PrintCustomers(repository);
            //   PrintCustomerById(repository,1);
            //   PrintCustomerByName(repository,"Peeters");

            // InsertCustomer(repository);

            // UddateCustomer(repository);
            // PrintCustomerByCountry(repository);
            // PrintCostumerSpendings(repository);
            PrinthighestSpendingGenreForCustomer(repository,42);
        }

        static void PrintCustomers(ICustomerRepository repository)
        {

            List<Customer> list = new List<Customer>();
            list = repository.GetAllCustomers();

            foreach (Customer customer in list)
            {

                Console.WriteLine($" CustomerID: {customer.CustomerId}, Firstname: {customer.FirstName}, Lastname: {customer.LastName}, Country: {customer.Country}," +
                    $"PostalCode:{customer.PostalCode}, Phone: {customer.Phone}, Email: {customer.Email}");



            }
        }

        static void PrintCustomerById(ICustomerRepository repository, int id)
        {


            Customer customer = new Customer();

            customer = repository.GetcustomerByID(id);


            string printString = $" CustomerID: {customer.CustomerId}, Firstname: {customer.FirstName}, Lastname: {customer.LastName}, Country: {customer.Country}," +
                   $"PostalCode:{customer.PostalCode}, Phone: {customer.Phone}, Email: {customer.Email}";

            Console.WriteLine(printString);
        }

        static void PrintCustomerByName(ICustomerRepository repository, string name)
        {


            Customer customer = new Customer();

            customer = repository.GetcustomerByName(name);


            string printString = $" CustomerID: {customer.CustomerId}, Firstname: {customer.FirstName}, Lastname: {customer.LastName}, Country: {customer.Country}," +
                   $"PostalCode:{customer.PostalCode}, Phone: {customer.Phone}, Email: {customer.Email}";

            Console.WriteLine(printString);
        }

        static void InsertCustomer(ICustomerRepository repository)
        {

            Customer customer = new Customer()
            {

                // CustomerId = 60,
                FirstName = "Bob",
                LastName = "Nilsen",
                Company = "UPS",
                Address = "Elvgata 2",
                City = "Bergen",
                State = "Hordalan",
                Country = "Norway",
                PostalCode = "4375",
                Phone = "999 333 444",
                Fax = "123123",
                Email = "asad@sadsa.com"

            };

            if (repository.AddNewCustomer(customer))
            {
                Console.WriteLine("Successfully inserted record");
                PrintCustomerByName(repository, customer.LastName);

            }
            else
            {
                Console.WriteLine("Not successful");
            }
        }

        static void UddateCustomer(ICustomerRepository repository)
        {
            Customer customer = new Customer()
            {

                CustomerId = 62,
                FirstName = "Kåre",
                LastName = "Gundersen",
                Company = "UPS",
                Address = "Elvgata 2",
                City = "Bergen",
                State = "Hordalan",
                Country = "Norway",
                PostalCode = "4375",
                Phone = "999 333 444",
                Fax = "123123",
                Email = "asad@sadsa.com"

            };

            if (repository.UpdateCustomer(customer))
            {
                Console.WriteLine("Successfully Updated record");
                PrintCustomerByName(repository, customer.LastName);

            }
            else
            {
                Console.WriteLine("Not successful");
            }
        }

        static void PrintCustomerByCountry(ICustomerRepository repository)
        {
            List<CustomerAmountByCountry> list = new List<CustomerAmountByCountry>();
            list = repository.GetCustomerByCountry();

            foreach (CustomerAmountByCountry customer in list)
            {

                Console.WriteLine($" Amount: {customer.Amount}, Country: {customer.Country}");



            }
        }

        static void PrintCostumerSpendings(ICustomerRepository repository)
        {
            List<CostumerSpendings> list = new List<CostumerSpendings>();
            list = repository.GetCustomerSpending();

            foreach (CostumerSpendings customer in list)
            {

                Console.WriteLine($" ID: {customer.CustomerId}, FirstName: {customer.FirstName}, LastName: {customer.LastName}, Total= {customer.Total}");



            }
        }

        static void PrinthighestSpendingGenreForCustomer(ICustomerRepository repository,int id)
        {
            List<SpendingsGenre> list = new List<SpendingsGenre>();
            list = repository.GetMostpopularGenre(id);

            if (list[0] != list[1])
            {
                Console.WriteLine($" Total Amount: {list[0].Spending} Genre: {list[1].Genre} ");
            }
            else
            {
                Console.WriteLine($" Total Amount: {list[0].Spending} Genre: {list[0].Genre} ");
                Console.WriteLine($" Total Amount: {list[1].Spending} Genre: {list[1].Genre} ");
            }



        }
    }
}
