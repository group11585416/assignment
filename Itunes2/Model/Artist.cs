﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Model
{
    internal class Artist
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }

    }
}
