﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Model
{
    public class CustomerAmountByCountry
    {
        public int Amount { get; set; }
        public string Country { get; set; }

    }
}
