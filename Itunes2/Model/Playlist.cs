﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itunes2.Model
{
    public class Playlist
    {
        public int PlaylistId { get; set; }
        public string Name { get; set; }

    }
}
