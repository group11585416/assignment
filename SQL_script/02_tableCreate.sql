use SuperheroesDb

create table SuperHero(
ID int primary key identity(1,1),
HeroName nvarchar(50),
Alias nvarchar(50),
Origin nvarchar(50)
)

create table Assistant(
ID int primary key identity(1,1),
AssistantName nvarchar(50),
)
create table Powers(
ID int primary key identity(1,1),
PowerName nvarchar(50),
Descriptions nvarchar(200),
)


