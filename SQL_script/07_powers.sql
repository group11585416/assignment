use SuperheroesDb

insert into Powers( PowerName, Descriptions) values('superpunch','punches really hard');
insert into Powers( PowerName, Descriptions) values('superKick','Kickes really hard');
insert into Powers( PowerName, Descriptions) values('fly','can fly like a bird');
insert into Powers( PowerName, Descriptions) values('superSpeed','Very fast');
insert into Powers( PowerName, Descriptions) values('Jump','Can jump high');


select HeroName as First_name
from SuperHero
INNER JOIN Powers
ON SuperHero.HeroName=Powers.PowerName;


--on student.profid=professor.id
--select student.First_name as strudent_name,
--Professor.First_name as professor_name from student
--Inner Join professor on student.ProfID=professor.id

--select g.genrename,gui.guitardescription (column  name)
--From genre as g (from table and give it a new name as g)
--Inner Join On genreguitar as gg (table 2 and give it a new name as gg)
--on g.genreid=gg.genreid and gg.guitarid=3

--inner join guitar as gui on gui.guitarid=gg.guitarid

--SELECT column_name(s)
--FROM table1
--INNER JOIN table2
--ON table1.column_name = table2.column_name;

