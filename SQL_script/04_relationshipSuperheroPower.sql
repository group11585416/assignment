use SuperheroesDb


create table SuperHeroPowers(
superheroID int not null,
PowerID int not null

constraint PK_SuperHeroPowers Primary Key(superheroID,PowerID),
constraint FK_superHeroPowers_Superhero foreign key(SuperHeroID) references SuperHero(ID),
constraint FK_superHeroPowers_Powers foreign key(PowerID) references Powers(ID)
);


